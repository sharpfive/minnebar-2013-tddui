//
//  AppDelegate.h
//  TddUi
//
//  Created by Jaim Zuber on 4/3/13.
//  Copyright (c) 2013 Sharp Five Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MYViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) MYViewController *viewController;

@end

//
//  MYViewController.h
//  TddUi
//
//  Created by Jaim Zuber on 4/4/13.
//  Copyright (c) 2013 Sharp Five Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MYViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *button;

@property (weak, nonatomic) IBOutlet UILabel *label;
- (IBAction)buttonPressed:(id)sender;

@end

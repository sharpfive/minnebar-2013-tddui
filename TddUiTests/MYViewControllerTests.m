//
//  ViewControllerTests.m
//  TddUi
//
//  Created by Jaim Zuber on 4/3/13.
//  Copyright (c) 2013 Sharp Five Software. All rights reserved.
//

#import "MYViewControllerTests.h"
#import "MYViewController.h"

@implementation MYViewControllerTests
{

    MYViewController *viewController;

}
- (void)setUp
{
    [super setUp];
    
    // initialize our controller
    viewController = [[MYViewController alloc]init];
    
    //Create a view
    [viewController view];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testButtonIsConnected
{    
    STAssertNotNil(viewController.button, @"Button should be loaded");
}

- (void)testInitialLabelState
{
    STAssertNotNil(viewController.label, @"Label shouldn't be nil");
    STAssertTrue(viewController.label.text.length == 0, @"Label text should be empty");
    STAssertTrue(viewController.label.hidden, @"The label should be hidden");
}

- (void)testButtonPressHasAnAction
{
    UIButton *button = viewController.button;
    NSArray *actions = [button actionsForTarget:viewController forControlEvent:UIControlEventTouchUpInside];
    
    STAssertTrue(actions.count > 0, @"There should be some actions");
    STAssertEqualObjects([actions objectAtIndex:0], @"buttonPressed:", @"buttonPressedShould be connected");
    
}
//
- (void)testButtonPress
{
    [viewController buttonPressed:nil];
    
    STAssertTrue(viewController.label.text.length > 0, @"Label text should be set");
    STAssertFalse(viewController.label.hidden, @"Label should not be hidden");
}

@end
